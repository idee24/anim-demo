package com.safsms.anim_demo;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity implements View.OnClickListener{


    private TextView borderedRipple, borderlessRipple, customRipple;
    private Button explodeJava, explodeXml, slideJava, slideXml, fadeJava, fadeXml;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        borderlessRipple = findViewById(R.id.borderlessRipple);
        borderedRipple = findViewById(R.id.borderedRipple);
        customRipple = findViewById(R.id.customRipple);

        explodeJava = findViewById(R.id.explodeJava);
        explodeXml = findViewById(R.id.explodeXml);
        slideJava = findViewById(R.id.slideJava);
        slideXml = findViewById(R.id.slideXml);
        fadeJava = findViewById(R.id.fadeJava);
        fadeXml = findViewById(R.id.fadeXml);

        explodeXml.setOnClickListener(this);
        explodeXml.setOnClickListener(this);
        slideJava.setOnClickListener(this);
        slideXml.setOnClickListener(this);
        fadeJava.setOnClickListener(this);
        fadeXml.setOnClickListener(this);

        borderedRipple.setOnClickListener(this);
        borderlessRipple.setOnClickListener(this);
        customRipple.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){

            case R.id.explodeJava:
                break;
            case R.id.explodeXml:
                break;
            case R.id.slideJava:
                break;
            case R.id.slideXml:
                break;
            case R.id.fadeJava:
                break;
            case R.id.fadeXml:
                break;
            case R.id.borderedRipple:
                break;
            case R.id.borderlessRipple:
                break;
            case R.id.customRipple:
                break;
        }
    }
}
